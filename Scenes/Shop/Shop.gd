extends Control

onready var CoinsText = $Panel/Coins

onready var ShopItems = $Panel/ShopItems

onready var ItemInfo = $Panel/ItemInfo

onready var NoCoins = $Panel/NoCoins

onready var AnimatedCoins = $Panel/AnimatedCoins

var item_shop = {
	"Copper_Ore": {"Price": 5, "Text": "A chunk of copper ore"},
	"Iron_Ore": {"Price": 30, "Text": "A fine chunk of iron ore"},
	"Steel_Ore": {"Price": 100, "Text": "A rare chunk of steel ore"},
	"Vulcanite_Ore": {"Price": 200, "Text": "A mystical chunk of vulcanite ore"},
	"Prometheum_Ore": {"Price": 100, "Text": "A rare chunk of prometheum ore"},
	"Mithril_Ore": {"Price": 200, "Text": "A mystical chunk of mithril ore"},
	"Orichalcum_Ore": {"Price": 100, "Text": "A rare chunk of orichalcum ore"},
	"Quicksilver_Ore": {"Price": 200, "Text": "A mystical chunk of quicksilver ore"},
	"Healing_Potion": {"Price": 15, "Text": "Regular healing potion." + "\n" + "+ 10 health"},
	"Greater_Healing_Potion": {"Price": 75, "Text": "Much better healing potion." + "\n" + "+ 50 health"},
	"Ultimate_Healing_Potion": {"Price": 150, "Text": "The best healing potion." + "\n" + "+ 100 health"},
}

func _ready():
	NoCoins.visible = false
	get_shop_items_dict()
	_on_ShopItems_item_selected(0)
	AnimatedCoins.play("coins")

func update_coins():
	var bag = Inventory.get_bag()
	var coins = bag["Coin"]
	CoinsText.text = "Coin: " + str(coins)
	
func get_shop_items_dict():
	for keys in item_shop:
		ShopItems.add_item(keys)
	
func _on_ShopItems_item_selected(index):
	NoCoins.visible = false
	var item_name = ShopItems.get_item_text(index)
	var price = item_shop[item_name]["Price"]
	var text = item_shop[item_name]["Text"]
	var bag = Inventory.get_bag()
	var owned = bag[item_name]
	
	ItemInfo.clear()
	ItemInfo.add_text("Price: " + str(price) + "\n" + text + "\n" + "Owned: " + str(owned))

func _on_Buy_pressed():
	var item_name = ShopItems.get_item_text(ShopItems.get_selected_id())
	var price = item_shop[item_name]["Price"]
	var bag = Inventory.get_bag()
	var coins = bag["Coin"]
	
	if price <= coins:
		var current_coins = coins - price
		Inventory.set_coins(current_coins)
		Inventory.add_bag(item_name, 1)
		print(Inventory.get_bag())
		_on_ShopItems_item_selected(ShopItems.get_selected_id())
	
	else:
		NoCoins.visible = true

func _physics_process(delta):
	update_coins()


func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/Play Menu.tscn"))
