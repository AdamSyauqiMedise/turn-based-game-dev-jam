extends Node

var max_hp : int
var hp : int
var atk : int
var def : int
var spd : int

var stun : bool
var bleed : bool
var taunt : bool
var slow : bool

# Initialization
func initialize(stats : Base_Stats):
	max_hp = stats.max_hp
	hp = stats.max_hp
	atk = stats.atk
	def = stats.def
	spd = stats.spd
	stun = false
	bleed = false
	taunt = false
	slow = false

func equip(helmet : Base_Equipment, chestplate : Base_Equipment, boots : Base_Equipment, main_hand : Base_Equipment, off_hand : Base_Equipment):
	if helmet != null:
		max_hp += helmet.hp
		atk += helmet.atk
		def += helmet.def
		spd += helmet.spd
	if chestplate != null:
		max_hp += chestplate.hp
		atk += chestplate.atk
		def += chestplate.def
		spd += chestplate.spd
	if boots != null:
		max_hp += boots.hp
		atk += boots.atk
		def += boots.def
		spd += boots.spd
	if boots != null:
		max_hp += boots.hp
		atk += boots.atk
		def += boots.def
		spd += boots.spd
	if main_hand != null:
		max_hp += main_hand.hp
		atk += main_hand.atk
		def += main_hand.def
		spd += main_hand.spd
	if off_hand != null:
		max_hp += off_hand.hp
		atk += off_hand.atk
		def += off_hand.def
		spd += off_hand.spd
	hp = max_hp

# Health
func set_max_hp():
	hp = max_hp
	
func get_max_hp():
	return max_hp
	
func get_hp() -> int:
	return hp
	
func take_damage(value : int):
	var dmg_taken = value - def
	if dmg_taken < 1:
		dmg_taken = 1
	hp -= dmg_taken
	if hp < 0:
		hp = 0
	
func heal(value : int):
	hp += value
	if hp > max_hp:
		hp = max_hp
	
func zero_hp():
	if hp <= 0:
		return false
	return true
	
# Atk
func get_atk() -> int:
	return atk
	
# Def
func get_def() -> int:
	return def
	
# Spd
func get_spd() -> int:
	return spd
