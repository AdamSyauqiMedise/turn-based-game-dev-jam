extends Position2D

export var stats : Resource
export var helmet: Resource
export var chestplate : Resource
export var boots : Resource
export var main_hand : Resource
export var off_hand : Resource

var class_job : String

onready var character_stats = $Stats

onready var hp = $HP

onready var tween = $Tween

onready var animated_sprite = $Idle

onready var timer = $Timer

func _ready():
	character_stats.initialize(stats)
	class_job = stats.class_job
	var class_equipment = Inventory.ready_equip(class_job)
	if class_equipment != null:
		parse_equipments(class_equipment)
	character_stats.equip(helmet, chestplate, boots, main_hand, off_hand)
	#print(basic_stats())
	animated_sprite.play("idle")
#	Inventory.huha()

func update_hp():
	hp.text = str(character_stats.get_hp()) + " / " + str(character_stats.get_max_hp())
	
func parse_equipments(equipments : Dictionary):
	print("printing keys")
	for keys in equipments.keys():
		var res_item = equipments[keys]
		var resource_load = load("res://Resources/Equipments/" + res_item + ".tres")
		if resource_load != null:
			var location = resource_load.location
			if location == "helmet":
				add_equipment(resource_load, location)
			elif location == "chestplate":
				add_equipment(resource_load, location)
			elif location == "boots":
				add_equipment(resource_load, location)
			elif location == "main_hand":
				add_equipment(resource_load, location)
			elif location == "off_hand":
				add_equipment(resource_load, location)

func add_equipment(equipment: Resource, type : String):
	if type == "helmet":
		helmet = equipment
	if type == "chestplate":
		chestplate = equipment
	if type == "boots":
		boots = equipment
	if type == "main_hand":
		main_hand = equipment
	if type == "off_hand":
		off_hand = equipment

func remove_equipment(type : String):
	if type == "helmet":
		helmet = null
	if type == "chestplate":
		chestplate = null
	if type == "boots":
		boots = null
	if type == "main_hand":
		main_hand = null
	if type == "off_hand":
		off_hand = null
	
func get_class_job():
	return class_job

func attack():
	var atk_power = character_stats.get_atk()
	return atk_power
	
func heal(amount : int):
	character_stats.heal(amount)
	tween.interpolate_property(animated_sprite, "modulate", Color(1,1,1,1),
	Color(0,1,0,1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	timer.wait_time = 0.2
	timer.one_shot = true
	timer.start()
	yield(timer, "timeout")
	tween.interpolate_property(animated_sprite, "modulate", Color(0,1,0,1),
	Color(1,1,1,1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

func take_damage(value : int):
	var dmg_taken = character_stats.take_damage(value)
	tween.interpolate_property(animated_sprite, "modulate", Color(1,1,1,1),
	Color(1,0,0,1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	timer.wait_time = 0.2
	timer.one_shot = true
	timer.start()
	yield(timer, "timeout")
	tween.interpolate_property(animated_sprite, "modulate", Color(1,0,0,1),
	Color(1,1,1,1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	
func dead():
	tween.interpolate_property(animated_sprite, "modulate", Color(1,1,1,1),
	Color(0,0,0,1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	
func basic_stats():
	return ("Hp: " + str(character_stats.get_hp()) + "\n" + "Atk: " 
	+ str(character_stats.get_atk()) + "\n" + "Def: " + str(character_stats.get_def()) 
	+ "\n" + "Spd: " + str(character_stats.get_spd()))

func is_alive():
	return character_stats.zero_hp()
	
func skill():
	pass
	
func _physics_process(delta):
	update_hp()
