extends Control

var bag = {}

onready var RewardScene = $Panel/Rewards

func _ready():
	get_bag()
	add_rewards()
	print(Inventory.get_bag())

func get_bag():
	bag = Rewards.get_bag()
	
func add_rewards():
	for keys in bag:
		var value = bag[keys]
		RewardScene.add_text((keys + ": X" + str(value)) + "\n")
		Inventory.add_bag(keys, value)


func _on_Button_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/Play Menu.tscn"))
