extends Control

onready var Forgeables = $Panel/Forgeables

onready var ItemInfo = $Panel/ItemInfo

onready var NoMaterials = $Panel/NoMaterials

onready var OwnedMaterials = $Panel/Owned

var weapon_dict = {
	# Knight
	"_Spear": 2,
	"_Shield": 3,
	
	# Mage
	"_Staff": 2,
	"_Orb": 2,
	
	# Rogue
	"_Blade": 2,
	"_Knife": 2,
}

var armor_dict = {
	# Knight
	"_Helmet": 2,
	"_Boots": 2,
	"_Chestplate": 4,
	
	# Mage
	"_Hat": 2,
	"_Robe": 3,
	"_Shoes": 2,
	
	# Rogue
	"_Scarf": 2,
	"_Tunic": 3,
	"_Sandals": 2,
}

var ore_weapons = ["Copper", "Iron", "Steel", "Vulcanite", "Prometheum", "Mithril", "Orichalcum", "Quicksilver"]

var ore_armors = ["Iron", "Vulcanite", "Mithril", "Quicksilver"]

var forgeable_items = {
}

func _ready():
	NoMaterials.visible = false
	add_weapons()
	add_armors()
	get_forgeable_items()
	_on_Forgeables_item_selected(0)
	
func add_weapons():
	for keys in weapon_dict:
		var price = weapon_dict[keys]
		for ores in ore_weapons:
			var weapon_name = ores + keys
			var bar_name = get_bar_name(ores) 
			forgeable_items[weapon_name] = {bar_name : price}
			
func add_armors():
	for keys in armor_dict:
		var price = armor_dict[keys]
		for ores in ore_armors:
			var armor_name = ores + keys
			var bar_name = get_bar_name(ores)
			forgeable_items[armor_name] = {bar_name : price}
	
func get_forgeable_items():
	var sorted_keys : Array = forgeable_items.keys()
	sorted_keys.sort()
	for keys in sorted_keys:
		Forgeables.add_item(keys)
		
func get_bar_name(bar : String):
	return bar + "_Bar"
	
func get_raw_material_name(material : String):
	var arr = material.split("_")
	return arr[0]

func _on_Forgeables_item_selected(index):
	NoMaterials.visible = false
	var item_name = Forgeables.get_item_text(index)
	var material_name = get_raw_material_name(item_name)
	var bar_name = get_bar_name(material_name)
	var price = forgeable_items[item_name][bar_name]
	
	var resource_load = load("res://Resources/Equipments/" + item_name + ".tres")
	var hp = resource_load.hp
	var atk = resource_load.atk
	var def = resource_load.def
	var spd = resource_load.spd
	var class_job = resource_load.class_job	
	
	var bag = Inventory.get_bag()
	var owned_materials = bag[bar_name]
	
	ItemInfo.clear()
	ItemInfo.add_text("HP: " + str(hp) + "\n" + "Atk: " + str(atk) + "\n" + "Def: " + str(def) + "\n" + "Spd: " + str(spd) + "\n")
	ItemInfo.add_text(bar_name + " needed: " + str(price))
	OwnedMaterials.text = bar_name + ": " + str(owned_materials) 

func _on_Forge_pressed():
	var item_name = Forgeables.get_item_text(Forgeables.get_selected_id())
	var material_name = get_raw_material_name(item_name)
	var bar_name = get_bar_name(material_name)
	var price = forgeable_items[item_name][bar_name]
	
	var bag = Inventory.get_bag()
	var owned_materials = bag[bar_name]
	
	if price <= owned_materials:
		var current_owned_materials = owned_materials - price
		Inventory.set_item_bag(bar_name, current_owned_materials)
		Inventory.add_inventory(item_name)
		print(Inventory.get_bag())
		print(Inventory.get_full_inventory())
		OwnedMaterials.text = bar_name + ": " + str(owned_materials)
		_on_Forgeables_item_selected(Forgeables.get_selected_id())
		
	else:
		NoMaterials.visible = true


func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/Play Menu.tscn"))
