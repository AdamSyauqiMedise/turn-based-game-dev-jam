extends Control

onready var Craftables = $Panel/Craftables

onready var ItemInfo = $Panel/ItemInfo

onready var NoMaterials = $Panel/NoMaterials

onready var OwnedMaterials = $Panel/Owned

var craftable_items = {
	"Copper_Bar": {"Copper_Ore": 3, "Text": "A bar made of copper"},
	"Iron_Bar": {"Iron_Ore": 3, "Text": "A fine bar made of iron"},
	"Steel_Bar": {"Steel_Ore": 4, "Text": "A refined bar made of steel"},
	"Vulcanite_Bar": {"Vulcanite_Ore": 5, "Text": "A mystical bar made of vulcanite"},
	"Prometheum_Bar": {"Prometheum_Ore": 4, "Text": "A refined bar made of prometheum"},
	"Mithril_Bar": {"Mithril_Ore": 5, "Text": "A mystical bar made of mithril"},
	"Orichalcum_Bar": {"Orichalcum_Ore": 4, "Text": "A refined bar made of orichalcum"},
	"Quicksilver_Bar": {"Quicksilver_Ore": 5, "Text": "A mystical bar made of quicksilver"},
}

func _ready():
	NoMaterials.visible = false
	get_craftable_items()
	_on_Craftables_item_selected(0)
	#print(craftable_items.values())
	
func get_craftable_items():
	for keys in craftable_items:
		Craftables.add_item(keys)
		
func get_ore_name(bar : String):
	var names = bar.split("_")
	var ore_name = names[0] + "_Ore"
	return ore_name

func _on_Craftables_item_selected(index):
	NoMaterials.visible = false
	var item_name = Craftables.get_item_text(index)
	var ore_name = get_ore_name(item_name)
	var price = craftable_items[item_name][ore_name]
	var text = craftable_items[item_name]["Text"]
	var bag = Inventory.get_bag()
	var owned = bag[item_name]
	var owned_materials = bag[ore_name]
	
	ItemInfo.clear()
	ItemInfo.add_text(text + "\n" + ore_name + " needed: " + str(price) + "\n" + "Owned: " + str(owned))
	OwnedMaterials.text = ore_name + ": " + str(owned_materials) 

func _on_Craft_pressed():
	var item_name = Craftables.get_item_text(Craftables.get_selected_id())
	var ore_name = get_ore_name(item_name)
	var price = craftable_items[item_name][ore_name]
	var bag = Inventory.get_bag()
	var owned = bag[item_name]
	var owned_materials = bag[ore_name]
	
	if price <= owned_materials:
		var current_owned_materials = owned_materials - price
		Inventory.set_item_bag(ore_name, current_owned_materials)
		Inventory.add_bag(item_name, 1)
		print(Inventory.get_bag())
		OwnedMaterials.text = ore_name + ": " + str(owned_materials)
		_on_Craftables_item_selected(Craftables.get_selected_id())
		
	else:
		NoMaterials.visible = true


func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/Play Menu.tscn"))
