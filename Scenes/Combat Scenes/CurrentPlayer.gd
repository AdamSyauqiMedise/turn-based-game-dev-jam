extends Control

onready var character_name = $"Panel/Character Name"

onready var small_heal = $"Panel/Small Heal"

onready var mid_heal = $"Panel/Mid Heal"

onready var big_heal = $"Panel/Big Heal"

onready var cooldown = $Panel/CD

func set_character_name(text_name : String):
	character_name.text = text_name
	
func set_small_heal(amount : int):
	small_heal.text = str(amount)
	
func get_small_heal():
	return int(small_heal.text)
	
func set_mid_heal(amount : int):
	mid_heal.text = str(amount)

func get_mid_heal():
	return int(mid_heal.text)
	
func set_big_heal(amount : int):
	big_heal.text = str(amount)

func get_big_heal():
	return int(big_heal.text)
	
func set_cd(amount: int):
	cooldown.text = "CD: " + str(amount) + " turns"
