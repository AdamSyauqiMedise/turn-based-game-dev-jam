extends Node2D

var rewards = {}

var turn = 0

var num = 0

var line = 0

var knight_heal_cd = 0

var mage_heal_cd = 0

var rogue_heal_cd = 0

var game_state = true

var turn_order : Array

var enemies_classes : Array

var player_classes : Array

var current_global_player

var player_classname = ["Knight", "Rogue", "Mage"]

onready var TurnQueueNode = $TurnQueue

var WinScene = "Win_Scene"

var LoseScene = "Lose_Scene"

var EndScene = "End"

onready var timer = $Timer

onready var pointer = $Pointer

onready var selected_player = $Selected

onready var heal_potions = $"Heal/Use Heal"

onready var level = $Level

func _ready():
	level.visible = false
	for i in range(TurnQueueNode.get_child_count()):
		turn_order.append(TurnQueueNode.get_child(i))
	#print("Sorting Speed...")
	sort_spd()
	#print("Assigning Enemies...")
	assign_enemies()
	#print("Assigning Players...")
	assign_players()
	#print("Entering Combat...")
	add_rewards(get_level_name())
	set_physics_process(false)
	timer.wait_time = 0.2
	timer.one_shot = true
	timer.start()
	yield(timer, "timeout")
	set_physics_process(true)
	
func get_level_name():
	return level.text

func get_potions():
	var potions = {}
	var get_bag = Inventory.get_bag()
	potions["Healing_Potion"] = get_bag["Healing_Potion"]
	potions["Greater_Healing_Potion"] = get_bag["Greater_Healing_Potion"]
	potions["Ultimate_Healing_Potion"] = get_bag["Ultimate_Healing_Potion"]
	return potions

func add_potions(potions : Dictionary):
	heal_potions.set_small_heal(potions["Healing_Potion"])
	heal_potions.set_mid_heal(potions["Greater_Healing_Potion"])
	heal_potions.set_big_heal(potions["Ultimate_Healing_Potion"])
	
func add_character_class(class_job : String):
	heal_potions.set_character_name(class_job)
	
func add_rewards(level_name):
	var get_rewards = RewardsList.get_rewards(level_name)
	rewards = get_rewards
	
func give_rewards():
	Rewards.add_bag(rewards)
	print(get_level_name())
	if get_level_name() == "Level Boss":
		get_tree().change_scene(str("res://Scenes/Menu/" + EndScene + ".tscn"))
	else:
		get_tree().change_scene(str("res://Scenes/Win Lose Scenes/" + WinScene + ".tscn"))
	
func player_lose():
	get_tree().change_scene(str("res://Scenes/Win Lose Scenes/" + LoseScene + ".tscn"))
		
func assign_enemies():
	var n = len(turn_order)
	for i in range(n):
		var class_job = turn_order[i].get_class_job()
		if !(class_job in player_classname):
			enemies_classes.append(turn_order[i])

func assign_players():
	var n = len(turn_order)
	for i in range(n):
		var class_job = turn_order[i].get_class_job()
		if class_job in player_classname:
			player_classes.append(turn_order[i])

func sort_spd():
	var n = len(turn_order)
	for i in range(n):
		for j in range(0, n-i-1):
			if turn_order[j].get_child(1).get_spd() < turn_order[j+1].get_child(1).get_spd():
				var temp = turn_order[j]
				turn_order[j] = turn_order[j+1]
				turn_order[j+1] = temp
			elif turn_order[j].get_child(1).get_spd() == turn_order[j+1].get_child(1).get_spd():
				if turn_order[j+1].get_class_job() in player_classname:
					var temp = turn_order[j]
					turn_order[j] = turn_order[j+1]
					turn_order[j+1] = temp
	for i in range(len(turn_order)):
		print(turn_order[i].get_class_job())

func combat():
	if turn >= len(turn_order):
		turn = 0
		cd_heal()
	if game_state:
		if turn_order[turn].get_class_job() in player_classname:
			player_turn(turn_order[turn])
		else:
			enemy_turn(turn_order[turn])

func player_turn(player : Position2D):
	var turn_active = true
	var selected = enemies_classes[num]
	var selected_pos = selected.position
	pointer.position = selected_pos
	pointer.position.x = selected_pos.x + 30
	
	current_global_player = player
	
	var player_pos = player.position
	selected_player.position = player.position
	selected_player.position.y = player.position.y - 10
	
	var character_class = player.get_class_job()
	add_character_class(character_class)
	var potions = get_potions()
	add_potions(potions)
	
	if character_class == "Knight":
		set_ui_heal_cd(get_knight_heal_cd())
	elif character_class == "Mage":
		set_ui_heal_cd(get_mage_heal_cd())
	elif character_class == "Rogue":
		set_ui_heal_cd(get_rogue_heal_cd())
	
	if turn_active:
		if Input.is_action_just_pressed("ui_up"):
#			print("Turn order: " + str(turn))
#			print("Enemy: " + str(num))
#			print("up")
			num -= 1
			if num < 0:
				num = len(enemies_classes) - 1
			selected = enemies_classes[num]
#			print(selected.get_class_job())
#			print(selected.basic_stats())
		if Input.is_action_just_pressed("ui_down"):
#			print("Turn order: " + str(turn))
#			print("Enemy: " + str(num))
#			print("down")
			num += 1
			if num > len(enemies_classes) - 1:
				num = 0
			selected = enemies_classes[num]
#			print(selected.get_class_job())
#			print(selected.basic_stats())
		if Input.is_action_just_pressed("attack"):
#			print("Enemy: " + str(num))
#			print("atk")
			var atk = player.attack()
			selected.take_damage(atk)
			turn_active = false
			if !(selected.is_alive()):
				dead(selected)
			turn += 1
			num = 0
	
func enemy_turn(enemy : Position2D):
#	print("Turn order: " + str(turn))
#	print(enemy.get_class_job())
	var enemy_class = enemy.get_class_job()
	var num = randi() % len(player_classes)
	var selected = player_classes[num]
	var atk = enemy.attack()
	selected.take_damage(atk)
	if enemy_class == "Boss":
		var chance = randi() % 2
		print(chance)
		if chance == 1:
			enemy.heal(atk)
	if !(selected.is_alive()):
		dead(selected)
#	print(selected.get_class_job())
#	print(selected.basic_stats())
	turn += 1

func dead(battler : Position2D):
	turn_order.erase(battler)
	battler.dead()
	set_physics_process(false)
	timer.wait_time = 0.4
	timer.one_shot = true
	timer.start()
	yield(timer, "timeout")
	$TurnQueue.remove_child(battler)
	if battler.get_class_job() in player_classname:
		print(battler.get_class_job())
		player_classes.erase(battler)
		set_physics_process(true)
	else:
		print(battler.get_class_job())
		enemies_classes.erase(battler)
		set_physics_process(true)

func check_win_or_lose():
	if len(enemies_classes) == 0:
		return 1
	elif len(player_classes) == 0:
		return 2

func _physics_process(delta):
	var check_game = check_win_or_lose()
	if check_game == 1:
		game_state = false
		give_rewards()
	elif check_game == 2:
		game_state = false
		player_lose()
	combat()
	
func get_current_global_player():
	return current_global_player.get_class_job()
	
func set_ui_heal_cd(amount : int):
	heal_potions.set_cd(amount)
	
func get_knight_heal_cd():
	return knight_heal_cd
	
func set_knight_heal_cd():
	knight_heal_cd = 4

func get_mage_heal_cd():
	return mage_heal_cd

func set_mage_heal_cd():
	mage_heal_cd = 4
	
func get_rogue_heal_cd():
	return rogue_heal_cd
	
func set_rogue_heal_cd():
	rogue_heal_cd = 4
	
func cd_heal():
	knight_heal_cd -= 1
	if knight_heal_cd < 0:
		knight_heal_cd = 0
	mage_heal_cd -= 1
	if mage_heal_cd < 0:
		mage_heal_cd = 0
	rogue_heal_cd -= 1
	if rogue_heal_cd < 0:
		rogue_heal_cd = 0

func _on_Small_Heal_pressed():
	if heal_potions.get_small_heal() > 0:
		var class_job = get_current_global_player()
		if class_job == "Knight":
			if get_knight_heal_cd() == 0:
				current_global_player.heal(10)
				Inventory.reduce_bag("Healing_Potion")
				turn += 1
				num = 0
				set_knight_heal_cd()
		elif class_job == "Mage":
			if get_mage_heal_cd() == 0:
				current_global_player.heal(10)
				Inventory.reduce_bag("Healing_Potion")
				turn += 1
				num = 0
				set_mage_heal_cd()
		elif class_job == "Rogue":
			if get_rogue_heal_cd() == 0:
				current_global_player.heal(10)
				Inventory.reduce_bag("Healing_Potion")
				turn += 1
				num = 0
				set_rogue_heal_cd()

func _on_Mid_Heal_pressed():
	if heal_potions.get_mid_heal() > 0:
		var class_job = get_current_global_player()
		if class_job == "Knight":
			if get_knight_heal_cd() == 0:
				current_global_player.heal(50)
				Inventory.reduce_bag("Greater_Healing_Potion")
				turn += 1
				num = 0
				set_knight_heal_cd()
		elif class_job == "Mage":
			if get_mage_heal_cd() == 0:
				current_global_player.heal(50)
				Inventory.reduce_bag("Greater_Healing_Potion")
				turn += 1
				num = 0
				set_mage_heal_cd()
		elif class_job == "Rogue":
			if get_rogue_heal_cd() == 0:
				current_global_player.heal(50)
				Inventory.reduce_bag("Greater_Healing_Potion")
				turn += 1
				num = 0
				set_rogue_heal_cd()
		

func _on_Big_Heal_pressed():
	if heal_potions.get_big_heal() > 0:
		var class_job = get_current_global_player()
		if class_job == "Knight":
			if get_knight_heal_cd() == 0:
				current_global_player.heal(100)
				Inventory.reduce_bag("Ultimate_Healing_Potion")
				turn += 1
				num = 0
				set_knight_heal_cd()
		elif class_job == "Mage":
			if get_mage_heal_cd() == 0:
				current_global_player.heal(100)
				Inventory.reduce_bag("Ultimate_Healing_Potion")
				turn += 1
				num = 0
				set_mage_heal_cd()
		elif class_job == "Rogue":
			if get_rogue_heal_cd() == 0:
				current_global_player.heal(100)
				Inventory.reduce_bag("Ultimate_Healing_Potion")
				turn += 1
				num = 0
				set_rogue_heal_cd()
