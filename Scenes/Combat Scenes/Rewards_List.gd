extends Node

var rewards_list = {
	"Level 1": {"Coin": 20, "Copper_Ore": 12, "Iron_Ore": 4},
	"Level 2": {"Coin": 40, "Copper_Ore": 20, "Iron_Ore": 8},
	"Level 3": {"Coin": 60, "Copper_Ore": 16, "Iron_Ore": 16},
	"Level Attack 1": {"Coin": 100, "Steel_Ore": 6},
	"Level Attack 2": {"Coin": 160, "Steel_Ore": 24, "Vulcanite_Ore": 6},
	"Level Attack 3": {"Coin": 300, "Steel_Ore": 16, "Vulcanite_Ore": 16},
	"Level Defense 1": {"Coin": 100, "Prometheum_Ore": 12},
	"Level Defense 2": {"Coin": 160, "Prometheum_Ore": 24, "Mithril_Ore": 6},
	"Level Defense 3": {"Coin": 300, "Prometheum_Ore": 16, "Mithril_Ore": 16},
	"Level Speed 1": {"Coin": 100, "Orichalcum_Ore": 12},
	"Level Speed 2": {"Coin": 160, "Orichalcum_Ore": 24, "Quicksilver_Ore": 6},
	"Level Speed 3": {"Coin": 300, "Orichalcum_Ore": 16, "Quicksilver_Ore": 16},
	"Level Boss": {"Coin": 20000}
}

func get_rewards(level_name : String):
	return rewards_list[level_name]
