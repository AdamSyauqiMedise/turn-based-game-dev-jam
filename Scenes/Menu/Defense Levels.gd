extends Control

export var defense_1 : String
export var defense_2 : String
export var defense_3 : String
export var levels : String


func _on_Defense_1_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + defense_1 + ".tscn"))


func _on_Defense_2_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + defense_2 + ".tscn"))


func _on_Defense_3_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + defense_3 + ".tscn"))


func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + levels + ".tscn"))
