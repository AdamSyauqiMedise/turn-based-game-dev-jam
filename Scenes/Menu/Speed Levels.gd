extends Control

export var speed_1 : String
export var speed_2 : String
export var speed_3 : String
export var levels : String


func _on_Speed_1_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + speed_1 + ".tscn"))


func _on_Speed_2_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + speed_2 + ".tscn"))


func _on_Speed_3_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + speed_3 + ".tscn"))


func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + levels + ".tscn"))
