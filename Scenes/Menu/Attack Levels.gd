extends Control

export var attack_1 : String
export var attack_2 : String
export var attack_3 : String
export var levels : String


func _on_Attack_1_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + attack_1 + ".tscn"))


func _on_Attack_2_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + attack_2 + ".tscn"))


func _on_Attack_3_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + attack_3 + ".tscn"))


func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + levels + ".tscn"))
