extends Control

export var basic_1 : String
export var basic_2 : String
export var basic_3 : String
export var levels : String


func _on_Basic_1_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + basic_1 + ".tscn"))


func _on_Basic_2_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + basic_2 + ".tscn"))


func _on_Basic_3_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + basic_3 + ".tscn"))


func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + levels + ".tscn"))
