extends Control

export var boss : String
export var levels : String


func _on_Boss_pressed():
	get_tree().change_scene(str("res://Scenes/Combat Scenes/" + boss + ".tscn"))

func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + levels + ".tscn"))
