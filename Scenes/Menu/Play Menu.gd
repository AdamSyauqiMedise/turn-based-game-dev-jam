extends Control

export var levels : String

export var back : String

export var craft : String

export var shop : String

export var forge : String

export var equip : String


func _on_Levels_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + levels + ".tscn"))


func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + back + ".tscn"))


func _on_Craft_pressed():
	get_tree().change_scene(str("res://Scenes/Crafting/" + craft + ".tscn"))


func _on_Shop_pressed():
	get_tree().change_scene(str("res://Scenes/Shop/" + shop + ".tscn"))


func _on_Forge_pressed():
	get_tree().change_scene(str("res://Scenes/Crafting/" + forge + ".tscn"))


func _on_Equip_pressed():
	get_tree().change_scene(str("res://Scenes/Inventory/" + equip + ".tscn"))
