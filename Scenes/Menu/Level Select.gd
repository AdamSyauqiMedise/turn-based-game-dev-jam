extends Control

export var basic : String
export var attack : String
export var defense : String
export var speed : String
export var boss : String
export var back : String


func _on_Basic_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + basic + ".tscn"))


func _on_Attack_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + attack + ".tscn"))


func _on_Defense_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + defense + ".tscn"))


func _on_Speed_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + speed + ".tscn"))


func _on_Boss_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + boss + ".tscn"))


func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + back + ".tscn"))
