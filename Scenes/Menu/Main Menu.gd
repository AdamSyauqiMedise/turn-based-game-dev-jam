extends Control

export var play : String

export var tutorial : String

export var credits : String


func _on_Play_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + play + ".tscn"))


func _on_Tutorial_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + tutorial + ".tscn"))


func _on_Credits_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/" + credits + ".tscn"))
