extends VBoxContainer

var class_job = "Rogue"

onready var helmet = $Helmet
onready var chestplate = $Chestplate
onready var boots = $Boots
onready var main_hand = $Main_Hand
onready var off_hand = $Off_Hand

func get_available_inventory():
	var class_inventory = Inventory.get_inventory(class_job)
	return class_inventory
	
func _ready():
	helmet.clear()
	chestplate.clear()
	boots.clear()
	main_hand.clear()
	off_hand.clear()
	var class_equipment = get_available_inventory()
	for item in class_equipment:
		var equipment = load("res://Resources/Equipments/" + item + ".tres")
		if equipment.location == "helmet":
			helmet.add_item(item)
		elif equipment.location == "chestplate":
			chestplate.add_item(item)
		elif equipment.location == "boots":
			boots.add_item(item)
		elif equipment.location == "main_hand":
			main_hand.add_item(item)
		elif equipment.location == "off_hand":
			off_hand.add_item(item)

func _on_Ok_Rogue_pressed():
	var helmet_equip = helmet.get_item_text(helmet.get_selected_id())
	var chestplate_equip = chestplate.get_item_text(chestplate.get_selected_id())
	var boots_equip = boots.get_item_text(boots.get_selected_id())
	var main_hand_equip = main_hand.get_item_text(main_hand.get_selected_id())
	var off_hand_equip = off_hand.get_item_text(off_hand.get_selected_id())
	
	Inventory.add_equipment(class_job, helmet_equip, "helmet")
	Inventory.add_equipment(class_job, chestplate_equip, "chestplate")
	Inventory.add_equipment(class_job, boots_equip, "boots")
	Inventory.add_equipment(class_job, main_hand_equip, "main_hand")
	Inventory.add_equipment(class_job, off_hand_equip, "off_hand")
