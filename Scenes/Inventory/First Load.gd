extends Node

var first_load = false

func set_first_load():
	first_load = true

func get_first_load():
	return first_load
