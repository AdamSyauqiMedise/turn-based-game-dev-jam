extends Node

var knight_inventory = {
	"helmet": "",
	"chestplate": "",
	"boots": "",
	"main_hand": "",
	"off_hand": ""
}

var mage_inventory = {
	"helmet": "",
	"chestplate": "",
	"boots": "",
	"main_hand": "",
	"off_hand": ""
}

var rogue_inventory = {
	"helmet": "",
	"chestplate": "",
	"boots": "",
	"main_hand": "",
	"off_hand": ""
}

var inventory_list : Array

var bag = {
	"Coin": 0,
	"Copper_Ore": 0,
	"Copper_Bar": 0,
	"Iron_Ore": 0,
	"Iron_Bar": 0,
	"Steel_Ore": 0,
	"Steel_Bar": 0,
	"Vulcanite_Ore": 0,
	"Vulcanite_Bar": 0,
	"Prometheum_Ore": 0,
	"Prometheum_Bar": 0,
	"Mithril_Ore": 0,
	"Mithril_Bar": 0,
	"Orichalcum_Ore": 0,
	"Orichalcum_Bar": 0,
	"Quicksilver_Ore": 0,
	"Quicksilver_Bar": 0,
	"Healing_Potion": 0,
	"Greater_Healing_Potion": 0,
	"Ultimate_Healing_Potion": 0,
}

func _ready():
	first_load()
	#second_load()

func first_load():
	if !(FirstLoad.get_first_load()):
		# Knight
		inventory_list.append("Leather_Helmet")
		add_equipment("Knight", "Leather_Helmet", "helmet")
		inventory_list.append("Leather_Chestplate")
		add_equipment("Knight", "Leather_Chestplate", "chestplate")
		inventory_list.append("Leather_Boots")
		add_equipment("Knight", "Leather_Boots", "boots")
		inventory_list.append("Wooden_Spear")
		add_equipment("Knight", "Wooden_Spear", "main_hand")
		inventory_list.append("Wooden_Shield")
		add_equipment("Knight", "Wooden_Shield", "off_hand")
		
		# Rogue
		inventory_list.append("Leather_Scarf")
		add_equipment("Rogue", "Leather_Scarf", "helmet")
		inventory_list.append("Leather_Tunic")
		add_equipment("Rogue", "Leather_Tunic", "chestplate")
		inventory_list.append("Leather_Sandals")
		add_equipment("Rogue", "Leather_Sandals", "boots")
		inventory_list.append("Wooden_Blade")
		add_equipment("Rogue", "Wooden_Blade", "main_hand")
		inventory_list.append("Wooden_Knife")
		add_equipment("Rogue", "Wooden_Knife", "off_hand")
		
		# Mage
		inventory_list.append("Leather_Hat")
		add_equipment("Mage", "Leather_Hat", "helmet")
		inventory_list.append("Leather_Robe")
		add_equipment("Mage", "Leather_Robe", "chestplate")
		inventory_list.append("Leather_Shoes")
		add_equipment("Mage", "Leather_Shoes", "boots")
		inventory_list.append("Wooden_Staff")
		add_equipment("Mage", "Wooden_Staff", "main_hand")
		inventory_list.append("Wooden_Orb")
		add_equipment("Mage", "Wooden_Orb", "off_hand")
		
		FirstLoad.set_first_load()

func second_load():
	if !(FirstLoad.get_first_load()):
		# Knight
		inventory_list.append("Vulcanite_Helmet")
		add_equipment("Knight", "Vulcanite_Helmet", "helmet")
		inventory_list.append("Vulcanite_Chestplate")
		add_equipment("Knight", "Vulcanite_Chestplate", "chestplate")
		inventory_list.append("Vulcanite_Boots")
		add_equipment("Knight", "Vulcanite_Boots", "boots")
		inventory_list.append("Vulcanite_Spear")
		add_equipment("Knight", "Vulcanite_Spear", "main_hand")
		inventory_list.append("Vulcanite_Shield")
		add_equipment("Knight", "Vulcanite_Shield", "off_hand")
		
		# Rogue
		inventory_list.append("Vulcanite_Scarf")
		add_equipment("Rogue", "Vulcanite_Scarf", "helmet")
		inventory_list.append("Vulcanite_Tunic")
		add_equipment("Rogue", "Vulcanite_Tunic", "chestplate")
		inventory_list.append("Vulcanite_Sandals")
		add_equipment("Rogue", "Vulcanite_Sandals", "boots")
		inventory_list.append("Vulcanite_Blade")
		add_equipment("Rogue", "Vulcanite_Blade", "main_hand")
		inventory_list.append("Vulcanite_Knife")
		add_equipment("Rogue", "Vulcanite_Knife", "off_hand")
		
		# Mage
		inventory_list.append("Vulcanite_Hat")
		add_equipment("Mage", "Vulcanite_Hat", "helmet")
		inventory_list.append("Vulcanite_Robe")
		add_equipment("Mage", "Vulcanite_Robe", "chestplate")
		inventory_list.append("Vulcanite_Shoes")
		add_equipment("Mage", "Vulcanite_Shoes", "boots")
		inventory_list.append("Vulcanite_Staff")
		add_equipment("Mage", "Vulcanite_Staff", "main_hand")
		inventory_list.append("Vulcanite_Orb")
		add_equipment("Mage", "Vulcanite_Orb", "off_hand")
		
		Inventory.set_item_bag("Healing_Potion", 100)
		
		FirstLoad.set_first_load()
	
func add_bag(key : String, value : int):
	bag[key] += value
	
func set_item_bag(key : String, value: int):
	bag[key] = value
	
func reduce_bag(key : String):
	bag[key] -= 1
	
func set_coins(value : int):
	bag["Coin"] = value

func get_bag():
	return bag
	
func ready_equip(class_job : String):
	if class_job == "Knight":
		return knight_inventory
	elif class_job == "Mage":
		return mage_inventory
	elif class_job == "Rogue":
		return rogue_inventory

func get_item(item):
	var res_item = inventory_list.find(item)
	if res_item > -1:
		inventory_list.remove(res_item)
		return load("res://Resources/Equipments/" + res_item + ".tres")
	pass

func add_inventory(equipment : String):
	inventory_list.append(equipment)

func get_inventory(class_job : String):
	var class_inventory : Array
	for item in inventory_list:
		var equipment = load("res://Resources/Equipments/" + item + ".tres")
		if equipment != null:
			if equipment.class_job == class_job:
				class_inventory.append(item)
	return class_inventory

func get_full_inventory():
	return inventory_list
	
func add_equipment(character_class : String, equipment : String, location : String):
	if character_class == "Knight":
		var used_equipment = knight_inventory[location]
		knight_inventory[location] = equipment
	elif character_class == "Rogue":
		var used_equipment = rogue_inventory[location]
		rogue_inventory[location] = equipment
	elif character_class == "Mage":
		var used_equipment = mage_inventory[location]
		mage_inventory[location] = equipment

func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/Menu/Play Menu.tscn"))
