extends Resource

class_name Base_Stats

export var class_job : String

export var max_hp : int
export var atk : int
export var def : int
export var spd : int

var stun : bool
var bleed : bool
var taunt : bool
var slow : bool
