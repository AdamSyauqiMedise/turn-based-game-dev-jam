extends Resource

class_name Base_Equipment

export var name : String

export var location : String

export var class_job : String

export var hp : int
export var atk : int
export var def : int
export var spd : int
